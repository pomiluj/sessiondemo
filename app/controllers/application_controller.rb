class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  def authenticate
    if session[:user].nil? #if user is nil
      redirect_to session_login_path
    end
  end
end

class SessionController < ApplicationController
  def login
  end

  def logout
    session[:user] = nil
    redirect_to session_login_path
  end

  def authenticate
    Rails.logger.debug params
    username = params[:username]
    password = params[:password]
    if password == '1234'
      session[:user] = username #sets a cookie in the browser that says "I'm logged in"
      redirect_to employees_path
    else
      redirect_to session_login_path
    end
  end
end
